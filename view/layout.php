<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="/public/css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<title>Currency converter</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-8">
			<form class="form-horizontal converter-form" method="POST">
				<div class="form-group col-sm-9">
					<label for="inputFromCurrency" >Enter amount in dollars</label>
					<input type="text" class="form-control" id="inputFromCurrency" name="fromCurrency" placeholder="$" required>
				</div>
				<div class="form-group col-sm-9">
					<button type="submit" class="btn btn-primary">Convert</button>
				</div>
			</form>
			<div class="col-sm-9 result"><?php echo $result; ?></div>
		</div>
	</div>
</div>

</body>
</html>