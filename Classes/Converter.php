<?php
	namespace Classes;
	
	Class Converter {
		
		public function getCource($url, $params) {
			$curl = new \Curl\Curl();
	
			$query = $curl->get($url, $params);
			
			$course = json_decode($query->response, true);
			
			return $course['USD_CNY']['val'];
		}
		
		public function convertation($course, $inputCurrency) {
			
			$result = $inputCurrency * $course;
			
			return $result;
		}
	}
?>