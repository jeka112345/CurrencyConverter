<?php

class HomeController {

    public function indexAction($params) {
        
		$url = 'http://free.currencyconverterapi.com/api/v5/convert';
		$params = array(
						'q' => 'USD_CNY',
						'compact' => 'y'
						);
		
		if (!empty($_POST['fromCurrency'])) {
		
			$course = new \Classes\Converter;
			$currentCource = $course->getCource($url, $params);
	
			$result = $course->convertation($currentCource, $_POST['fromCurrency']);
			
		}
		
		// Render view
        include 'view/layout.php';
    }
}